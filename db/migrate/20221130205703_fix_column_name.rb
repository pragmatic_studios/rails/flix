class FixColumnName < ActiveRecord::Migration[7.0]
  def self.up
    rename_column :movies, :gross, :total_gross
  end
end
