module ApplicationHelper
  def total_gross(movie)
    movie.flop? ? 'Flop' : number_to_currency(movie.total_gross)
  end

  def year_of(movie)
    #movie.released_on.strftime('%Y')
    movie.released_on.year
  end
end
