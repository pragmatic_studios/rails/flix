class Movie < ApplicationRecord
  def flop?
    flop_amount = 550
    total_gross.blank? || total_gross < flop_amount
  end
end
